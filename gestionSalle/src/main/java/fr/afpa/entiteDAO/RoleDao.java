package fr.afpa.entiteDAO;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="role")
public class RoleDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
	@SequenceGenerator(name = "role_generator", sequenceName = "role_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idrole", updatable = false, nullable = false )
	
	private int idRole;
	
	@Column
	private String libelle;
	
	@OneToMany(mappedBy = "role")
	@OnDelete(action=OnDeleteAction.CASCADE)
	private List<PersonneDao> personne;

	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<PersonneDao> getPersonne() {
		return personne;
	}

	public void setPersonne(List<PersonneDao> personne) {
		this.personne = personne;
	}

}
