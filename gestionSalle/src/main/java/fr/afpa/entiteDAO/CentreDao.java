package fr.afpa.entiteDAO;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="centre")
public class CentreDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "centre_generator")
	@SequenceGenerator(name = "centre_generator", sequenceName = "centre_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idcentre", updatable = false, nullable = false )
	private int idCentre;
	
	@Column
	private String nom;
	
	@OneToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "idAdresse")
	private AdresseDao adresse;
	
	@OneToMany(mappedBy = "centre")
	@OnDelete(action=OnDeleteAction.CASCADE)
	private List<PersonneDao> personne;

	public int getIdCentre() {
		return idCentre;
	}

	public void setIdCentre(int idCentre) {
		this.idCentre = idCentre;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public AdresseDao getAdresse() {
		return adresse;
	}

	public void setAdresse(AdresseDao adresse) {
		this.adresse = adresse;
	}

	public List<PersonneDao> getPersonne() {
		return personne;
	}

	public void setPersonne(List<PersonneDao> personne) {
		this.personne = personne;
	}
	
	
}
