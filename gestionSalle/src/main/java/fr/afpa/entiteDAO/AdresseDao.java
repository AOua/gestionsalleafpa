package fr.afpa.entiteDAO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "adresse")
public class AdresseDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adresse_generator")
	@SequenceGenerator(name = "adresse_generator", sequenceName = "adresse_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idadresse", updatable = false, nullable = false )
	private Integer idAdresse;
	
	@Column
	private Integer numero;

	@Column
	private String rue;

	@Column
	private String ville;
	
	@Column
	private String codePostal;
	
	@OneToOne(mappedBy="adresse")
	private PersonneDao personne;
	
	@OneToOne(mappedBy="adresse")
	private CentreDao centre;

	public Integer getIdAdresse() {
		return idAdresse;
	}

	public void setIdAdresse(Integer idAdresse) {
		this.idAdresse = idAdresse;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public PersonneDao getPersonne() {
		return personne;
	}

	public void setPersonne(PersonneDao personne) {
		this.personne = personne;
	}

	public CentreDao getCentre() {
		return centre;
	}

	public void setCentre(CentreDao centre) {
		this.centre = centre;
	}
	


}
