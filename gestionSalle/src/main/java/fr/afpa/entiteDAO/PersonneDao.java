package fr.afpa.entiteDAO;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="personne")
public class PersonneDao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personne_generator")
	@SequenceGenerator(name = "personne_generator", sequenceName = "personne_seq",allocationSize = 1, initialValue = 2)
	@Column (name="idpersonne", updatable = false, nullable = false )
	private int idPersonne;
	
	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@Column
	private String telephone;
	
	@Column
	private String email;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true )
	@OnDelete( action = OnDeleteAction.CASCADE )
	@JoinColumn(name = "idAdresse")
	private AdresseDao adresse;	
	
	@ManyToOne(cascade = { CascadeType.PERSIST })
	@OnDelete( action = OnDeleteAction.CASCADE )
	@JoinColumn(name = "idRole")
	private RoleDao role;
	
	@ManyToOne(cascade = { CascadeType.PERSIST })
	@OnDelete( action = OnDeleteAction.CASCADE )
	@JoinColumn(name = "idCentre")
	private CentreDao centre;
	
	
	@OneToOne(mappedBy="personne", cascade = CascadeType.ALL, orphanRemoval = true )
	@OnDelete( action = OnDeleteAction.CASCADE )
	private AuthentificationDao authentification;


	public int getIdPersonne() {
		return idPersonne;
	}


	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getTelephone() {
		return telephone;
	}


	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public AdresseDao getAdresse() {
		return adresse;
	}


	public void setAdresse(AdresseDao adresse) {
		this.adresse = adresse;
	}


	public RoleDao getRole() {
		return role;
	}


	public void setRole(RoleDao role) {
		this.role = role;
	}


	public CentreDao getCentre() {
		return centre;
	}


	public void setCentre(CentreDao centre) {
		this.centre = centre;
	}


	public AuthentificationDao getAuthentification() {
		return authentification;
	}


	public void setAuthentification(AuthentificationDao authentification) {
		this.authentification = authentification;
	}
	
}
