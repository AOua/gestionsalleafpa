package fr.afpa.entityMetier;

public class Authentification {

	private int idAuthentification;
	private String login;
	private String motDePasse;
	private Personne personne;
	
	public Authentification() {
		super();
	}

	public Authentification(int idAuthentification, String login, String motDePasse, Personne personne) {
		super();
		this.idAuthentification = idAuthentification;
		this.login = login;
		this.motDePasse = motDePasse;
		this.personne = personne;
	}

	@Override
	public String toString() {
		return "Authentification [idAuthentification=" + idAuthentification + ", login=" + login + ", motDePasse="
				+ motDePasse + ", personne=" + personne + "]";
	}

	public int getIdAuthentification() {
		return idAuthentification;
	}

	public void setIdAuthentification(int idAuthentification) {
		this.idAuthentification = idAuthentification;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	
	
}
