package fr.afpa.entityMetier;

import java.util.List;

public class Centre {

	private int idCentre;
	private String nom;
	private Adresse adresse;
	private List<Personne> personne;

	public Centre() {
		super();
	}

	public Centre(int idCentre, String nom, Adresse adresse, List<Personne> personne) {
		super();
		this.idCentre = idCentre;
		this.nom = nom;
		this.adresse = adresse;
		this.personne = personne;
	}

	@Override
	public String toString() {
		return "Centre [idCentre=" + idCentre + ", nom=" + nom + "]";
	}

	public int getIdCentre() {
		return idCentre;
	}

	public void setIdCentre(int idCentre) {
		this.idCentre = idCentre;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public List<Personne> getPersonne() {
		return personne;
	}

	public void setPersonne(List<Personne> personne) {
		this.personne = personne;
	}
	
	
}
