package fr.afpa.entityMetier;

import java.util.List;

public class Role {

	private int idRole;
	private String libelle;
	private List<Personne> personne;
	
	public Role() {
		super();
	}

	public Role(int idRole, String libelle, List<Personne> personne) {
		super();
		this.idRole = idRole;
		this.libelle = libelle;
		this.personne = personne;
	}

	@Override
	public String toString() {
		return "Role [idRole=" + idRole + ", libelle=" + libelle + ", personne=" + personne + "]";
	}

	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Personne> getPersonne() {
		return personne;
	}

	public void setPersonne(List<Personne> personne) {
		this.personne = personne;
	}

	
}
