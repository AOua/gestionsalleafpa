package fr.afpa.entityMetier;

public class Adresse {

	private Integer idAdresse;
	private Integer numero;
	private String rue;
	private String ville;
	private String codePostal;
	private Personne personne;
	private Centre centre;

	public Adresse() {
		super();
	}

	public Adresse(Integer idAdresse, Integer numero, String rue, String ville, String codePostal, Personne personne,
			Centre centre) {
		super();
		this.idAdresse = idAdresse;
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
		this.personne = personne;
		this.centre = centre;
	}

	@Override
	public String toString() {
		return "Adresse [idAdresse=" + idAdresse + ", numero=" + numero + ", rue=" + rue + ", ville=" + ville
				+ ", codePostal=" + codePostal + ", personne=" + personne + ", centre=" + centre + "]";
	}

	public Integer getIdAdresse() {
		return idAdresse;
	}

	public void setIdAdresse(Integer idAdresse) {
		this.idAdresse = idAdresse;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public Centre getCentre() {
		return centre;
	}

	public void setCentre(Centre centre) {
		this.centre = centre;
	}
	
	
	
}
