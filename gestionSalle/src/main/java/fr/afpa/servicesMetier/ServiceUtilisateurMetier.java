package fr.afpa.servicesMetier;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entityMetier.Adresse;
import fr.afpa.entityMetier.Authentification;
import fr.afpa.entityMetier.Centre;
import fr.afpa.entityMetier.Personne;
import fr.afpa.entityMetier.Role;
import fr.afpa.servicesDTO.ServiceUtilisateurDTO;
import fr.afpa.utils.HibernateUtils;

public class ServiceUtilisateurMetier {
	
	public boolean creationUtilisateur(Personne personne, Adresse adresse, Authentification authentification) {
		ServiceUtilisateurDTO serviceUtilisateurDto = new ServiceUtilisateurDTO();
		return serviceUtilisateurDto.addUtilisateur(personne, authentification, adresse); 
}
	
	public Personne afficherUtilisateur(int idPersonne) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.showUtilisateur(idPersonne);
		
	}
	
	public boolean modifierPersonne(Personne personne) {
		
		ServiceUtilisateurDTO serviceUtilisateurDto = new ServiceUtilisateurDTO();
		return serviceUtilisateurDto.modifyUtilisateur(personne);
	}
	
	public boolean supprimerUtilisateur(int idPersonne) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.deteleUtilisateur(idPersonne);
	}
	/*
	 * public boolean supprimerUtilisateur(Personne personne) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.deteleUtilisateur(personne);
	}
	 */
	
	public boolean validateAdmin(String login, String password) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.validationAdmin(login, password);
	}
	
	public boolean validateUser(String login, String password) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.validationUser(login, password);
	}
	
	public Personne getUserByLogin(String login, String password) {
		ServiceUtilisateurDTO serviceUtilisateurDTO = new ServiceUtilisateurDTO();
		return serviceUtilisateurDTO.getUserByLog(login, password);
	}
	
	public Role getRole(int idRole) {
		
		 ServiceUtilisateurDTO serviceUtilDto = new ServiceUtilisateurDTO();
		 return serviceUtilDto.getRole(idRole);
	}
	
	
	public Centre getCentre(int idCentre) {
		
		return new ServiceUtilisateurDTO().getCentre(idCentre);
	}
	
	public ArrayList<Personne> afficherListeUtilisateur() {
		
		ServiceUtilisateurDTO serv = new ServiceUtilisateurDTO();
		
		 return serv.listUtilisateur();	
		
	}

	

	
}