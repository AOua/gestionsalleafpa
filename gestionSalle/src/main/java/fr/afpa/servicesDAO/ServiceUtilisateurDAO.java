package fr.afpa.servicesDAO;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entiteDAO.AdresseDao;
import fr.afpa.entiteDAO.AuthentificationDao;
import fr.afpa.entiteDAO.CentreDao;
import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entiteDAO.RoleDao;
import fr.afpa.entityMetier.Personne;
import fr.afpa.utils.HibernateUtils;

public class ServiceUtilisateurDAO {

	/**
	 * Ajouter une personne dans la base de donnée
	 * 
	 * @param personne
	 * @param adresse
	 * @param authentification
	 */
	public boolean ajouterUtilisateur(PersonneDao personne, AdresseDao adresse, AuthentificationDao authentification) {

		boolean retour = false;
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			session.save(adresse);
			session.save(authentification);
			session.save(personne);
			transaction.commit();
			retour = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();

		}
		return retour;
	}

	/**
	 * Modifie l'utilisateur en base de donnée
	 * 
	 * @param personne
	 */
	public boolean modifierUtilisateurBdd(PersonneDao personne) {
		boolean retour = false;
		Session session = null;
		Transaction transaction = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			session.update(personne);
			transaction.commit();
			retour = true;
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return retour;
	}

	/**
	 * Supprimer un utilisateur
	 * 
	 * @param idPers
	 */
	public boolean supprimerUtilisateur(int idPers) {
		boolean retour = false;
		Session session = null;
		Transaction transaction = null;
		session = HibernateUtils.getSession();
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			PersonneDao personne = session.get(PersonneDao.class, idPers);
			if (personne != null && personne.getIdPersonne() != 1) {
				session.delete(personne);
			}
			transaction.commit();			
			retour = true;

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return retour;

	}

	/**
	 * Récupere une personne
	 * 
	 * @param idPers
	 * @return
	 */
	public PersonneDao afficherUtilisateur(int idPers) {
		Session session = null;
		Transaction transaction = null;
		PersonneDao personne = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			personne = session.get(PersonneDao.class, idPers);
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return personne;
	}

	/**
	 * Afficher la liste des utilisateurs
	 * 
	 * @return
	 */
	
	public ArrayList<PersonneDao> afficherListeUtilisateur() {
		Session session = null;
		Transaction transaction = null;
		ArrayList<PersonneDao> listeUtilisateur = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			listeUtilisateur = (ArrayList<PersonneDao>) session.createQuery("from PersonneDao p order by p.nom ").getResultList();
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return listeUtilisateur;
	}

	/**
	 * Si le login et mot de passe correspondent à l'admin dans la base de donnée
	 * alors on autorise la connection
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public boolean validateAdmin(String login, String password) {
		Session session = null;
		Transaction transaction = null;
		AuthentificationDao auth = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			auth = (AuthentificationDao) session.createQuery("FROM AuthentificationDao a WHERE a.login = :login")
					.setParameter("login", login).uniqueResult();
			PersonneDao personne = auth.getPersonne();
			RoleDao role = personne.getRole();
			int idRole = role.getIdRole();
			if (auth != null && auth.getMotDePasse().equals(password) && idRole == 1) {
				return true;
			}
			transaction.commit();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return false;
	}

	/**
	 * Si le login et mot de passe correspondent à l'user dans la base de donnée
	 * alors on autorise la connection
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public boolean validateUser(String login, String password) {
		Session session = null;
		Transaction transaction = null;
		AuthentificationDao auth = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			auth = (AuthentificationDao) session.createQuery("FROM AuthentificationDao a WHERE a.login = :login")
					.setParameter("login", login).uniqueResult();
			PersonneDao personne = auth.getPersonne();
			RoleDao role = personne.getRole();
			int idRole = role.getIdRole();
			if (auth != null && auth.getMotDePasse().equals(password) && idRole == 2) {
				return true;
			}
			transaction.commit();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return false;
	}

	/**
	 * Récupérer l'utilisateur qui se connecte
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	public PersonneDao getUser(String login, String password) {
		Session session = null;
		Transaction transaction = null;
		AuthentificationDao auth = null;
		PersonneDao personne = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();

			auth = (AuthentificationDao) session.createQuery("FROM AuthentificationDao a WHERE a.login = :login")
					.setParameter("login", login).uniqueResult();
			personne = auth.getPersonne();
			RoleDao role = personne.getRole();
			int idRole = role.getIdRole();
			if (auth != null && auth.getMotDePasse().equals(password) && idRole == 2) {
				return personne;
			}else if(auth != null && auth.getMotDePasse().equals(password) && idRole == 1) {
				return personne;
			}
			transaction.commit();
		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return personne;
	}

	/**
	 * Récupérer le role de l'utilisateur (admin ou user) selon son id
	 * 
	 * @param idRole
	 * @return
	 */
	public RoleDao getRole(int idRole) {

		Session session = null;
		Transaction transaction = null;
		RoleDao role = null;
		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			role = session.get(RoleDao.class, idRole);
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return role;
	}

	/**
	 * récupérer le centre grâce à son id
	 * 
	 * @param idCentre
	 * @return
	 */
	public CentreDao getCentre(int idCentre) {

		Session session = null;
		Transaction transaction = null;
		CentreDao centre = null;

		try {
			session = HibernateUtils.getSession();
			transaction = session.beginTransaction();
			centre = session.get(CentreDao.class, idCentre);
			transaction.commit();

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			session.close();
		}
		return centre;
	}
}
