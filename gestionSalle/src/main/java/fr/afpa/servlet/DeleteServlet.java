package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class DeleteServlet
 */
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaAdmin");
		if(personneSession != null) {
		ServiceUtilisateurMetier su = new ServiceUtilisateurMetier();
		if(request.getParameter("supprimer")!=null) {
			String idRequest = request.getParameter("id");
			int idDelete = Integer.parseInt(idRequest);
			Personne personne = su.afficherUtilisateur(idDelete);
			su.supprimerUtilisateur(idDelete);
			
			     RequestDispatcher dispatcher = request.getRequestDispatcher("adminPanel");
		            dispatcher.forward(request, response);
		        
			}else {   	
	        	RequestDispatcher dispatcher = request.getRequestDispatcher("adminPanel");
	            dispatcher.forward(request, response);
	        }		
	}else {
		RequestDispatcher dispatcher = request.getRequestDispatcher("");
        dispatcher.forward(request, response);
	}
	}

}
