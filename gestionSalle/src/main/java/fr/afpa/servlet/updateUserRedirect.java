package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesDAO.ServiceUtilisateurDAO;

/**
 * Servlet implementation class updateUserRedirect
 */
public class updateUserRedirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateUserRedirect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaUser");
		if(personneSession != null) {
		if(request.getParameter("update") != null) {
			String idRequest = request.getParameter("id");
			int idModif = Integer.parseInt(idRequest);
			ServiceUtilisateurDAO su = new ServiceUtilisateurDAO();
			
			PersonneDao personne = su.afficherUtilisateur(idModif);
			if(personne != null) {
				
				 String nom = request.getParameter("nom");
				 personne.setNom(nom);
				 String prenom = request.getParameter("prenom");
				 personne.setPrenom(prenom);
				 String tel = request.getParameter("tel");
				 personne.setTelephone(tel);
				 String email = request.getParameter("email");
				 personne.setEmail(email);
				 String numAdresse = request.getParameter("numAdr");
				 int numAdr = Integer.parseInt(numAdresse);
				 personne.getAdresse().setNumero(numAdr);
				 String nomAdr = request.getParameter("nomAdr");
				 personne.getAdresse().setRue(nomAdr);
				 String cp = request.getParameter("cp");
				 personne.getAdresse().setCodePostal(cp);
				 String ville = request.getParameter("ville");
				 personne.getAdresse().setVille(ville);
				// String idCentre = request.getParameter("centre");
				// int idAfpa = Integer.parseInt(idCentre);
				 personne.getCentre().setIdCentre(1);
			
				 String login = request.getParameter("login");
				 personne.getAuthentification().setLogin(login);
				 String password = request.getParameter("password");
				 personne.getAuthentification().setMotDePasse(password);
				 
				 su.modifierUtilisateurBdd(personne);
				 RequestDispatcher dispatcher = request.getRequestDispatcher("updateUserOK.jsp");
				 dispatcher.forward(request, response);
			}
			else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("updateUser.jsp");
	            dispatcher.forward(request, response);
			}
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
            dispatcher.forward(request, response);
		}
		}
		else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("");
            dispatcher.forward(request, response);
		}
	}

}
