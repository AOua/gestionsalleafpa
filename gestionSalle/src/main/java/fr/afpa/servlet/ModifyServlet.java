package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class ModifyServlet
 */
public class ModifyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaAdmin");
		if(personneSession != null) {
		ServiceUtilisateurMetier su = new ServiceUtilisateurMetier();
		if(request.getParameter("update") != null) {
			String idRequest = request.getParameter("id");
			int idModif = Integer.parseInt(idRequest);
			
			
			Personne personne = su.afficherUtilisateur(idModif);
			if(personne != null) {
				
				 String nom = request.getParameter("nom");
				 personne.setNom(nom);
				 String prenom = request.getParameter("prenom");
				 personne.setPrenom(prenom);
				 String tel = request.getParameter("tel");
				 personne.setTelephone(tel);
				 String email = request.getParameter("email");
				 personne.setEmail(email);
				 String numAdresse = request.getParameter("numAdr");
				 int numAdr = Integer.parseInt(numAdresse);
				 personne.getAdresse().setNumero(numAdr);
				 String nomAdr = request.getParameter("nomAdr");
				 personne.getAdresse().setRue(nomAdr);
				 String cp = request.getParameter("cp");
				 personne.getAdresse().setCodePostal(cp);
				 String ville = request.getParameter("ville");
				 personne.getAdresse().setVille(ville);
				 personne.getCentre().setIdCentre(1);
			
				 String login = request.getParameter("login");
				 personne.getAuthentification().setLogin(login);
				 String password = request.getParameter("password");
				 personne.getAuthentification().setMotDePasse(password);
				 
				 if(su.modifierPersonne(personne)) {
					 RequestDispatcher dispatcher = request.getRequestDispatcher("updateOK.jsp");
					 dispatcher.forward(request, response);
				 }else {
						RequestDispatcher dispatcher = request.getRequestDispatcher("adminPanel");
			            dispatcher.forward(request, response);
				 }
			}
			else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("adminPanel");
	            dispatcher.forward(request, response);
			}
		}
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("");
            dispatcher.forward(request, response);
		}
	}

}

