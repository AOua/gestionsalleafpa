package fr.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class adminPanel
 */
public class adminPanel extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public adminPanel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaAdmin");
		if(personneSession != null) {
			showUsers(request,response);
		}else {
		   	RequestDispatcher dispatcher = request.getRequestDispatcher("");
		    dispatcher.forward(request, response);	 
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
		
	}
	
	/**
	 * Servlet qui envoit une liste de personne vers une page jsp
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void showUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	  
		ServiceUtilisateurMetier sum = new ServiceUtilisateurMetier();
		ArrayList<Personne> listePersonne = new ArrayList<Personne>();
		listePersonne = sum.afficherListeUtilisateur();
	   	 request.setAttribute("listUser", listePersonne);
	   	 if(listePersonne!=null) {
	   		 RequestDispatcher dispatcher = request.getRequestDispatcher("showAllUser.jsp");
	            dispatcher.forward(request, response);
	   	 }else {
	   		 RequestDispatcher dispatcher = request.getRequestDispatcher("accueil.jsp");
	            dispatcher.forward(request, response);
	   	 }
	    
	}
	
	

}
