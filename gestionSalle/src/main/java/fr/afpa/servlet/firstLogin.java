package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class firstLogin
 */
public class firstLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public firstLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("adminPanel");
        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
            authenticate(request, response);
       
	        
	}
	
	private void authenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			HttpSession session = request.getSession();
	    	String login = request.getParameter("login");
		    String password = request.getParameter("password");
		    ServiceUtilisateurMetier su = new ServiceUtilisateurMetier();
		    String error = "Login ou mot de passe incorrect";
		    
			

			if (su.validateAdmin(login, password)) {
				Personne personne = su.getUserByLogin(login, password);
				session.setAttribute("holaAdmin", personne);
	            RequestDispatcher dispatcher = request.getRequestDispatcher("accueil.jsp");
	            dispatcher.forward(request, response);
	        }
			else if (su.validateUser(login, password)) {
				Personne personne = su.getUserByLogin(login, password);
				session.setAttribute("holaUser", personne);
	            RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
	            dispatcher.forward(request, response);
			}
			else {
	        	request.setAttribute("error", error);        	
	        	RequestDispatcher dispatcher = request.getRequestDispatcher("");
	            dispatcher.forward(request, response);
	        }
	    
	}

}
