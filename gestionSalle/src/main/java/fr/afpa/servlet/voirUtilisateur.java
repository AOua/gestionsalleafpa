package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesDAO.ServiceUtilisateurDAO;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class voirUtilisateur
 */
public class voirUtilisateur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public voirUtilisateur() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaUser");
		if(personneSession != null) {
		ServiceUtilisateurMetier serviceMetierAffichage = new ServiceUtilisateurMetier();
		if(request.getParameter("voir")!=null) {
			String idRequest = request.getParameter("id");
			int idVoir = Integer.parseInt(idRequest);
			
			Personne personne = serviceMetierAffichage.afficherUtilisateur(idVoir);
			request.setAttribute("user", personne);
			if(personne!=null) {

			     RequestDispatcher dispatcher = request.getRequestDispatcher("voirUser.jsp");
		            dispatcher.forward(request, response);
		        
			}else {   	
	        	RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
	            dispatcher.forward(request, response);
	        }
		}
		else {   	
        	RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
            dispatcher.forward(request, response);
        }
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("");
            dispatcher.forward(request, response);
		}
	}

}
