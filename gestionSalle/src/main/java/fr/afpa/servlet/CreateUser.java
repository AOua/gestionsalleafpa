package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entiteDAO.AdresseDao;
import fr.afpa.entiteDAO.AuthentificationDao;
import fr.afpa.entiteDAO.CentreDao;
import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entiteDAO.RoleDao;
import fr.afpa.entityMetier.Adresse;
import fr.afpa.entityMetier.Authentification;
import fr.afpa.entityMetier.Centre;
import fr.afpa.entityMetier.Personne;
import fr.afpa.entityMetier.Role;
import fr.afpa.servicesMetier.ServiceUtilisateurMetier;

/**
 * Servlet implementation class Create
 */
public class CreateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaAdmin");
		if(personneSession != null) {
		ServiceUtilisateurMetier serviceMetierCreation = new ServiceUtilisateurMetier();

		if (request.getParameter("role").equals("1")) {
			int idRole = Integer.parseInt(request.getParameter("role"));
			String idCentre = request.getParameter("centre");
			int idAfpa = 0;
			if(controlCentre(idCentre)) {
				idAfpa = Integer.parseInt(idCentre);
			}else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("createUser.jsp");
				dispatcher.forward(request, response);
			}
			Role role = serviceMetierCreation.getRole(idRole);
			Centre centre = serviceMetierCreation.getCentre(idAfpa);
			Personne personne = new Personne();
			Adresse adresse = new Adresse();
			Authentification authentification = new Authentification();
			personne.setRole(role);
			String nom = request.getParameter("nom");
			personne.setNom(nom);
			String prenom = request.getParameter("prenom");
			personne.setPrenom(prenom);
			String tel = request.getParameter("tel");
			personne.setTelephone(tel);
			String email = request.getParameter("email");
			personne.setEmail(email);
			String numAdresse = request.getParameter("numAdr");
			int numAdr = Integer.parseInt(numAdresse);
			adresse.setNumero(numAdr);
			String nomAdr = request.getParameter("nomAdr");
			adresse.setRue(nomAdr);
			String cp = request.getParameter("cp");
			adresse.setCodePostal(cp);
			String ville = request.getParameter("ville");
			adresse.setVille(ville);
			String login = request.getParameter("login");
			authentification.setLogin(login);
			String password = request.getParameter("password");
			authentification.setMotDePasse(password);
			
			personne.setAdresse(adresse);
			personne.setAuthentification(authentification);
			adresse.setPersonne(personne);
			
			authentification.setPersonne(personne);
			personne.setAuthentification(authentification);		
			personne.setCentre(centre);
			serviceMetierCreation.creationUtilisateur(personne, adresse, authentification);
			RequestDispatcher dispatcher = request.getRequestDispatcher("createAdminOK.jsp");
			dispatcher.forward(request, response);

		} else if (request.getParameter("role").equals("2")) {
			int idRole = Integer.parseInt(request.getParameter("role"));
			String idCentre = request.getParameter("centre");
			int idAfpa = 0;
			if(controlCentre(idCentre)) {
				idAfpa = Integer.parseInt(idCentre);
			}else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("createUser.jsp");
				dispatcher.forward(request, response);
			}
			
			Role role = serviceMetierCreation.getRole(idRole);
			Centre centre = serviceMetierCreation.getCentre(idAfpa);
			Personne personne = new Personne();
			Adresse adresse = new Adresse();
			Authentification authentification = new Authentification();
			personne.setRole(role);
			String nom = request.getParameter("nom");
			personne.setNom(nom);
			String prenom = request.getParameter("prenom");
			personne.setPrenom(prenom);
			String tel = request.getParameter("tel");
			personne.setTelephone(tel);
			String email = request.getParameter("email");
			personne.setEmail(email);
			String numAdresse = request.getParameter("numAdr");
			int numAdr = Integer.parseInt(numAdresse);
			adresse.setNumero(numAdr);
			String nomAdr = request.getParameter("nomAdr");
			adresse.setRue(nomAdr);
			String cp = request.getParameter("cp");
			adresse.setCodePostal(cp);
			String ville = request.getParameter("ville");
			adresse.setVille(ville);
			String login = request.getParameter("login");
			authentification.setLogin(login);
			String password = request.getParameter("password");
			authentification.setMotDePasse(password);
			
			personne.setAdresse(adresse);
			personne.setAuthentification(authentification);
			adresse.setPersonne(personne);
			
			authentification.setPersonne(personne);
			personne.setAuthentification(authentification);		
			personne.setCentre(centre);
			serviceMetierCreation.creationUtilisateur(personne, adresse, authentification);
			RequestDispatcher dispatcher = request.getRequestDispatcher("createUserOK.jsp");
			dispatcher.forward(request, response);
		}
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("accueil.jsp");
			dispatcher.forward(request, response);
		}

	}
	
	public static boolean controlCentre(String idCentre) {
		String pattern = "[0-9]+";
		if(idCentre.matches(pattern)) {
			return true;
		}
		return false;
	}

}
