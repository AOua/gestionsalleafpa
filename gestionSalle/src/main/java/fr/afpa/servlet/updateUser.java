package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entityMetier.Personne;
import fr.afpa.servicesDAO.ServiceUtilisateurDAO;

/**
 * Servlet implementation class updateUser
 */
public class updateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Personne personneSession = (Personne) session.getAttribute("holaUser");
		if(personneSession != null) {
		ServiceUtilisateurDAO su = new ServiceUtilisateurDAO();
		if(request.getParameter("modifier")!=null) {
			String idRequest = request.getParameter("id");
			int idModif = Integer.parseInt(idRequest);
			
			
			PersonneDao personne = su.afficherUtilisateur(idModif);
			if(personne!=null) {
				 int id = personne.getIdPersonne();
				 request.setAttribute("id", id);
				 String nom = personne.getNom();
				 request.setAttribute("nom", nom);
				 String prenom = personne.getPrenom();
				 request.setAttribute("prenom", prenom);
				 String tel = personne.getTelephone();
				 request.setAttribute("tel", tel);
				 String email = personne.getEmail();
				 request.setAttribute("email", email);
				 int numAdr = personne.getAdresse().getNumero();
				 request.setAttribute("numAdr", numAdr);
				 String nomAdr = personne.getAdresse().getRue();
				 request.setAttribute("nomAdr", nomAdr);
				 String cp = personne.getAdresse().getCodePostal();
				 request.setAttribute("cp", cp);
				 String ville = personne.getAdresse().getVille();
				 request.setAttribute("ville", ville);
				 int idCentre = personne.getCentre().getIdCentre();
				 request.setAttribute("idCentre", idCentre);
				 int nomCentre = personne.getCentre().getIdCentre();
				 request.setAttribute("nomCentre", nomCentre);
				 String login = personne.getAuthentification().getLogin();
				 request.setAttribute("login", login);
				 String password = personne.getAuthentification().getMotDePasse();
				 request.setAttribute("password", password);
				 
			     RequestDispatcher dispatcher = request.getRequestDispatcher("updateUser.jsp");
		            dispatcher.forward(request, response);
		        
			}else {   	
	        	RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
	            dispatcher.forward(request, response);
	        }
		}
		else {   	
        	RequestDispatcher dispatcher = request.getRequestDispatcher("accueilUser.jsp");
            dispatcher.forward(request, response);
        }
		}else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("");
            dispatcher.forward(request, response);
		}
	}

}
