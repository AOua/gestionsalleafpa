package fr.afpa.servicesDTO;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entiteDAO.AdresseDao;
import fr.afpa.entiteDAO.AuthentificationDao;
import fr.afpa.entiteDAO.CentreDao;
import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entiteDAO.RoleDao;
import fr.afpa.entityMetier.Adresse;
import fr.afpa.entityMetier.Authentification;
import fr.afpa.entityMetier.Centre;
import fr.afpa.entityMetier.Personne;
import fr.afpa.entityMetier.Role;
import fr.afpa.servicesDAO.ServiceUtilisateurDAO;

public class ServiceUtilisateurDTO {

	public boolean addUtilisateur(Personne personne, Authentification authentification, Adresse adresse) {
		ServiceUtilisateurDAO serviceUtilisateurDao = new ServiceUtilisateurDAO();

		PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
		AuthentificationDao authentificationDao = mappingAuthentificationMetierToAuthentificationDAO(authentification);
		AdresseDao adresseDao = mappingAdressenMetierToAdresseDAO(adresse);
		adresseDao.setPersonne(personneDao);
		personneDao.setAdresse(adresseDao);

		authentificationDao.setPersonne(personneDao);
		personneDao.setAuthentification(authentificationDao);
		return serviceUtilisateurDao.ajouterUtilisateur(personneDao, adresseDao, authentificationDao);
	}

	public Personne showUtilisateur(int idPersonne) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		PersonneDao personneDao = serviceUtilisateurDAO.afficherUtilisateur(idPersonne);
		Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
		return personne;

	}

	public Personne getUserByLog(String login, String password) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		PersonneDao personneDao = serviceUtilisateurDAO.getUser(login, password);
		Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
		return personne;
	}

	public boolean deteleUtilisateur(int idPersonne) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		return serviceUtilisateurDAO.supprimerUtilisateur(idPersonne);
	}

	// alternative
	/*
	 * public boolean deleteUtilisateur(int idPersonne) { ServiceUtilisateurDAO
	 * serviceUtilisateurDAO = new ServiceUtilisateurDAO(); PersonneDao personneDao
	 * = return serviceUtilisateurDAO.supprimerUtilisateur(idPersonne); }
	 */

	public boolean validationAdmin(String login, String password) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		return serviceUtilisateurDAO.validateAdmin(login, password);
	}

	public boolean validationUser(String login, String password) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		return serviceUtilisateurDAO.validateUser(login, password);
	}

	
	  public boolean modifyUtilisateur(Personne personne) { ServiceUtilisateurDAO
	  serviceUtilisateurDao = new ServiceUtilisateurDAO(); PersonneDao personneDao
	  = mappingPersonneMetierToPersonneDao(personne);
	  
	  AdresseDao adresseDao =
	  mappingAdressenMetierToAdresseDAO(personne.getAdresse());
	  adresseDao.setNumero(personne.getAdresse().getNumero());
	  adresseDao.setRue(personne.getAdresse().getRue());
	  adresseDao.setCodePostal(personne.getAdresse().getCodePostal());
	  adresseDao.setVille(personne.getAdresse().getVille());
	  personneDao.setAdresse(adresseDao);
	  
	  AuthentificationDao authentificationDao =
	  mappingAuthentificationMetierToAuthentificationDAO(
	  personne.getAuthentification());
	  authentificationDao.setLogin(personne.getAuthentification().getLogin());
	  authentificationDao.setMotDePasse(personne.getAuthentification().
	  getMotDePasse()); personneDao.setAuthentification(authentificationDao);
	  
	  
	  CentreDao centreDao = mappingCentreMetierToCentreDao(personne.getCentre());
	  centreDao.setIdCentre(personne.getCentre().getIdCentre());
	  personneDao.setCentre(centreDao);
	  
	  // adresseDao.setPersonne(personneDao); //
	  authentificationDao.setPersonne(personneDao); //
	  personneDao.setAuthentification(authentificationDao); return
	  serviceUtilisateurDao.modifierUtilisateurBdd(personneDao); }
	 
/*
	public boolean modifyUtilisateur(Personne personne) {
		ServiceUtilisateurDAO serviceUtilisateurDao = new ServiceUtilisateurDAO();
		PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
		
		int numAdresse = personne.getAdresse().getNumero();
		String nomRue = personne.getAdresse().getRue();
		String cp = personne.getAdresse().getCodePostal();
		String ville = personne.getAdresse().getVille();
		
		personneDao.getAdresse().setNumero(numAdresse);
		personneDao.getAdresse().setRue(nomRue);
		personneDao.getAdresse().setVille(ville);
		personneDao.getAdresse().setCodePostal(cp);

		personneDao.getCentre().setIdCentre(1);

		String login = personne.getAuthentification().getLogin();
		String mdp = personne.getAuthentification().getMotDePasse();

		personneDao.getAuthentification().setLogin(login);
		personneDao.getAuthentification().setMotDePasse(mdp);

		return serviceUtilisateurDao.modifierUtilisateurBdd(personneDao);

	}*/

	public ArrayList<Personne> listUtilisateur() {
		ServiceUtilisateurDAO serviceUtilisateurDao = new ServiceUtilisateurDAO();
		ArrayList<PersonneDao> listeUtilisateurDao = serviceUtilisateurDao.afficherListeUtilisateur();
		ArrayList<Personne> listeUtilisateurMetier = null;
		if (listeUtilisateurDao != null) {
			listeUtilisateurMetier = (ArrayList<Personne>) mappingListPersonneDaoToListPersonneMetier(
					listeUtilisateurDao);
			return listeUtilisateurMetier;
		}
		return listeUtilisateurMetier;
	}

	public Role getRole(int idRole) {
		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		Role role = new Role();
		RoleDao roleDao = serviceUtilisateurDAO.getRole(idRole);
		role.setIdRole(roleDao.getIdRole());
		role.setLibelle(roleDao.getLibelle());

		return role;

	}

	public Centre getCentre(int idCentre) {
		AdresseDao adresseDao = null;

		ServiceUtilisateurDAO serviceUtilisateurDAO = new ServiceUtilisateurDAO();
		Centre centre = new Centre();
		CentreDao centreDao = serviceUtilisateurDAO.getCentre(idCentre);
		adresseDao = centreDao.getAdresse();
		centre.setIdCentre(centreDao.getIdCentre());

		Adresse adresse = new Adresse();
		adresse.setIdAdresse(adresseDao.getIdAdresse());
		adresse.setNumero(adresseDao.getNumero());
		adresse.setRue(adresseDao.getRue());
		adresse.setCodePostal(adresseDao.getCodePostal());
		adresse.setCentre(centre);
		adresse.setVille(adresseDao.getVille());

		centre.setAdresse(adresse);
		centre.setNom(centreDao.getNom());
		return centre;

	}

	public AuthentificationDao mappingAuthentificationMetierToAuthentificationDAO(Authentification authentification) {

		AuthentificationDao authentificationDao = new AuthentificationDao();

		authentificationDao.setIdAuthentification(authentification.getIdAuthentification());
		authentificationDao.setLogin(authentification.getLogin());
		authentificationDao.setMotDePasse(authentification.getMotDePasse());
		return authentificationDao;
	}

	public Authentification mappingAuthentificationDaoToAuthentification(AuthentificationDao authentificationDao) {

		Authentification authentification = new Authentification();

		authentification.setIdAuthentification(authentificationDao.getIdAuthentification());
		authentification.setLogin(authentificationDao.getLogin());
		authentification.setMotDePasse(authentificationDao.getMotDePasse());
		return authentification;
	}

	public AdresseDao mappingAdressenMetierToAdresseDAO(Adresse adresse) {

		AdresseDao adresseDao = new AdresseDao();

		adresseDao.setIdAdresse(adresse.getIdAdresse());
		adresseDao.setNumero(adresse.getNumero());
		adresseDao.setRue(adresse.getRue());
		adresseDao.setCodePostal(adresse.getCodePostal());
		adresseDao.setVille(adresse.getVille());
		return adresseDao;

	}

	public Adresse mappingAdressenDaoToAdresse(AdresseDao adresseDao) {

		Adresse adresse = new Adresse();

		adresse.setIdAdresse(adresseDao.getIdAdresse());
		adresse.setNumero(adresseDao.getNumero());
		adresse.setRue(adresseDao.getRue());
		adresse.setCodePostal(adresseDao.getCodePostal());
		adresse.setVille(adresseDao.getVille());
		return adresse;
	}

	public Personne mappingPersonneDaoToPersonneMetier(PersonneDao personneDao) {
		Personne personne = new Personne();

		personne.setIdPersonne(personneDao.getIdPersonne());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setEmail(personneDao.getEmail());
		personne.setTelephone(personneDao.getTelephone());
		personne.setAdresse(mappingAdressenDaoToAdresse(personneDao.getAdresse()));
		personne.setAuthentification(mappingAuthentificationDaoToAuthentification(personneDao.getAuthentification()));
		personne.setRole(mappingRoleDaoToRoleMetier(personneDao.getRole()));
		personne.setCentre(mappingCentreDaoToCentreMetier(personneDao.getCentre()));
		return personne;
	}

	public PersonneDao mappingPersonneMetierToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setIdPersonne(personne.getIdPersonne());
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setEmail(personne.getEmail());
		personneDao.setTelephone(personne.getTelephone());
		personneDao.setRole(mappingRoleMetierToRoleDao(personne.getRole()));
		personneDao.setCentre(mappingCentreMetierToCentreDao(personne.getCentre()));
		return personneDao;

	}

	public RoleDao mappingRoleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();

		roleDao.setIdRole(role.getIdRole());
		roleDao.setLibelle(role.getLibelle());
		return roleDao;
	}

	public Role mappingRoleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role();

		role.setIdRole(roleDao.getIdRole());
		role.setLibelle(roleDao.getLibelle());
		return role;
	}

	public Centre mappingCentreDaoToCentreMetier(CentreDao centreDao) {
		Centre centre = new Centre();

		centre.setIdCentre(centreDao.getIdCentre());
		centre.setNom(centreDao.getNom());
		return centre;
	}

	public CentreDao mappingCentreMetierToCentreDao(Centre centre) {
		CentreDao centreDao = new CentreDao();

		centreDao.setIdCentre(centre.getIdCentre());
		centreDao.setNom(centre.getNom());
		return centreDao;
	}

	public ArrayList<PersonneDao> mappingListPersonneMetierToListPersonneDao(List<Personne> listPersonne) {
		ArrayList<PersonneDao> listPersonneDao = new ArrayList<PersonneDao>();

		for (Personne personne : listPersonne) {
			PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
			listPersonneDao.add(personneDao);
		}

		return listPersonneDao;
	}

	public List<Personne> mappingListPersonneDaoToListPersonneMetier(List<PersonneDao> listPersonneDao) {
		ArrayList<Personne> listPersonne = new ArrayList<Personne>();

		for (PersonneDao personneDao : listPersonneDao) {
			Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
			listPersonne.add(personne);
		}

		return listPersonne;
	}

}
