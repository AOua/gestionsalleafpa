package fr.afpa.servicesDTO;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.entiteDAO.AdresseDao;
import fr.afpa.entiteDAO.AuthentificationDao;
import fr.afpa.entiteDAO.CentreDao;
import fr.afpa.entiteDAO.PersonneDao;
import fr.afpa.entiteDAO.RoleDao;
import fr.afpa.entityMetier.Adresse;
import fr.afpa.entityMetier.Authentification;
import fr.afpa.entityMetier.Centre;
import fr.afpa.entityMetier.Personne;
import fr.afpa.entityMetier.Role;

public class ServiceUtilisateurDTOMapping {
	
public AuthentificationDao mappingAuthentificationMetierToAuthentificationDAO (Authentification authentification) {
		
		AuthentificationDao authentificationDao = new AuthentificationDao();

		authentificationDao.setIdAuthentification(authentification.getIdAuthentification());
		authentificationDao.setLogin(authentification.getLogin());
		authentificationDao.setMotDePasse(authentification.getMotDePasse());
		authentificationDao.setPersonne(mappingPersonneMetierToPersonneDao(authentification.getPersonne()));
		return authentificationDao;
	}
	public Authentification mappingAuthentificationDaoToAuthentification (AuthentificationDao authentificationDao) {
		
		Authentification authentification = new Authentification();

		authentification.setIdAuthentification(authentificationDao.getIdAuthentification());
		authentification.setLogin(authentificationDao.getLogin());
		authentification.setMotDePasse(authentificationDao.getMotDePasse());
		return authentification;
	}
	

	public AdresseDao mappingAdressenMetierToAdresseDAO (Adresse adresse) {
		
		AdresseDao adresseDao = new AdresseDao();

		adresseDao.setIdAdresse(adresse.getIdAdresse());
		adresseDao.setNumero(adresse.getNumero());
		adresseDao.setRue(adresse.getRue());
		adresseDao.setCodePostal(adresse.getCodePostal());
		adresseDao.setVille(adresse.getVille());
		return adresseDao;
		
	}
public Adresse mappingAdressenDaoToAdresse (AdresseDao adresseDao) {
		
		Adresse adresse = new Adresse();

		adresse.setIdAdresse(adresseDao.getIdAdresse());
		adresse.setNumero(adresseDao.getNumero());
		adresse.setRue(adresseDao.getRue());
		adresse.setCodePostal(adresseDao.getCodePostal());
		adresse.setVille(adresseDao.getVille());
		return adresse;
	}
	
	
	public Personne mappingPersonneDaoToPersonneMetier (PersonneDao personneDao) {
		Personne personne = new Personne();

		personne.setIdPersonne(personneDao.getIdPersonne());
		personne.setNom(personneDao.getNom());
		personne.setPrenom(personneDao.getPrenom());
		personne.setEmail(personneDao.getEmail());
		personne.setTelephone(personneDao.getTelephone());
		personne.setRole(mappingRoleDaoToRoleMetier(personneDao.getRole()));
		personne.setCentre(mappingCentreDaoToCentreMetier(personneDao.getCentre()));
		return personne;
	}
	
	public PersonneDao mappingPersonneMetierToPersonneDao (Personne personne) {
		PersonneDao personneDao = new PersonneDao();

		personneDao.setIdPersonne(personne.getIdPersonne());
		personneDao.setNom(personne.getNom());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setEmail(personne.getEmail());
		personneDao.setTelephone(personne.getTelephone());
		personneDao.setRole(mappingRoleMetierToRoleDao(personne.getRole()));
		personneDao.setCentre(mappingCentreMetierToCentreDao(personne.getCentre()));
		return personneDao;
		
	}
	
	public RoleDao mappingRoleMetierToRoleDao (Role role) {
		RoleDao roleDao = new RoleDao();

		roleDao.setIdRole(role.getIdRole());
		roleDao.setLibelle(role.getLibelle());
		return roleDao;	
	} 
	public Role mappingRoleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role();

		role.setIdRole(roleDao.getIdRole());
		role.setLibelle(roleDao.getLibelle());
		return role;	
	} 
	public Centre mappingCentreDaoToCentreMetier (CentreDao centreDao) {
		Centre centre = new Centre();

		centre.setIdCentre(centreDao.getIdCentre());
		centre.setNom(centreDao.getNom());
		return centre;	
	} 
	public CentreDao mappingCentreMetierToCentreDao (Centre centre) {
		CentreDao centreDao = new CentreDao();

		centreDao.setIdCentre(centre.getIdCentre());
		centreDao.setNom(centre.getNom());
		return centreDao;	
	} 
	
	public List<PersonneDao> mappingListPersonneMetierToListPersonneDao (List<Personne> listPersonne) {
		ArrayList<PersonneDao> listPersonneDao = new ArrayList<PersonneDao>();
		
		for (Personne personne : listPersonne) {
			PersonneDao personneDao = mappingPersonneMetierToPersonneDao(personne);
			listPersonneDao.add(personneDao);
		}

		return listPersonneDao;
	}
	
	public List<Personne> mappingListPersonneDaoToListPersonneMetier (List<PersonneDao> listPersonneDao) {
		ArrayList<Personne> listPersonne = new ArrayList<Personne>();
		
		for (PersonneDao personneDao : listPersonneDao) {
			Personne personne = mappingPersonneDaoToPersonneMetier(personneDao);
			listPersonne.add(personne);
		}

		return listPersonne;
	}
	
}
