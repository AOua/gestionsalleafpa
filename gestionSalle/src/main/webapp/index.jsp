<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>

<div class="container-fluid bg">
<div class="row justify-content-center">
<div class="container-fluid col-sm-5">
	<h2>Hello World</h2>
	<form method="post" action="firstLogin" class="form-container">
		<div class="form-group">
		    <label for="login">Login : </label>
		    <input type="text" class="form-control" name="login" id="login" placeholder="Entrer votre login" required>
		</div>
		    <div class="form-group">
		    <label for="password">Mot de passe :</label>
		    <input type="password" class="form-control" name="password" id="password" placeholder="Entrer votre mot de passe" required>
		</div>
		<button type="submit" class="btn btn-primary">Valider</button>
	</form>
		<c:if test = "${not empty error}" >
			<div class="bg-danger"><c:out value="${error}" /></div>
		</c:if>
</div>
</div>
</div>

<script src="js/jquery-3.4.1.slim.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
