<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid col-sm-12">
	<p>Voici la liste de tous les utilisateurs</p>
	<table class="table table-striped" >
  <thead>
    <tr>
      <th>Id</th>
      <th>Nom</th>
      <th>Prenom</th>
      <th>Tel</th>
      <th>Email</th>
      <th>Adresse</th>
      <th>Centre AFPA</th>
      <th>Login</th>
      <th>Mot de passe</th>
      <th>Role</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  	<c:if test="${ not empty listUser }" >
	  	<c:forEach items="${ listUser }" var="element">
	  		 <tr>
		      <td><c:out value="${ element.idPersonne }"></c:out></td>
		      <c:set var ="id" value="${ element.idPersonne }"/>
		      <td><c:out value="${ element.nom }"></c:out></td>
		      <td><c:out value="${ element.prenom }"></c:out></td>
		      <td><c:out value="${ element.telephone }"></c:out></td>
		      <td><c:out value="${ element.email }"></c:out></td>
		      <td><c:out value="${ element.adresse.numero } ${element.adresse.rue} ${element.adresse.codePostal} ${element.adresse.ville }"></c:out></td>
		      <td><c:out value="${ element.centre.nom }"></c:out></td>
		      <td><c:out value="${ element.authentification.login }"></c:out></td>
		      <td><c:out value="${ element.authentification.motDePasse}"></c:out></td>
		      <td><c:out value="${ element.role.libelle }"></c:out></td>
		      <td>
		     	 <div class="btn-group" role="group">
			      	<form class="form" action="ShowUser" method="post">
			      	<input type="hidden" name = "id" value="<c:out value="${ id }" />">
			      	<input type="submit" class="btn btn-primary" name = "voir" value="voir">
			      	</form>
			      	<form class="form" action="updateServlet" method="post">
			      	<input type="hidden" name = "id" value="<c:out value="${ id }" />">
			      	<input type="submit" class="btn btn-light" name = "modifier" value="modifier">
			      	</form>
			      	<form class="form" action="DeleteServlet" method="post">
			      	<input type="hidden" name = "id" value="<c:out value="${ id }" />">
			      	<input type="submit" class="btn btn-danger" name = "supprimer" value="supprimer">
			      	</form>
		      	</div>
		      </td>
		   	 </tr>
	  	</c:forEach>
	</c:if>
   
  </tbody>
</table>
	  <a href="accueil.jsp" class="btn btn-primary">retour</a>
</div>
<script src="js/jquery-3.4.1.slim.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
