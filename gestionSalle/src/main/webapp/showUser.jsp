<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<jsp:include page="nav.jsp"/>
	<div class="container-fluid col-sm-12">
		<p>Infos Utilisateur</p>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>Tel</th>
					<th>Email</th>
					<th>Adresse</th>
					<th>Centre AFPA</th>
					<th>Login</th>
					<th>Mot de passe</th>
					<th>Role</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${ not empty user }">



					<tr>

						
						<td><c:out value="${user.nom}"></c:out></td>
						<td><c:out value="${user.prenom}"></c:out></td>
						<td><c:out value="${user.telephone}"></c:out></td>
						<td><c:out value="${user.email}"></c:out></td>


						
						<td><c:out
								value="${user.adresse.numero} ${user.adresse.rue } ${user.adresse.codePostal } ${user.adresse.ville }"></c:out>
						</td>
						<td><c:out value="${user.centre.nom}"></c:out></td>

						<td><c:out value="${user.authentification.login }"></c:out></td>
						<td><c:out value="${user.authentification.motDePasse }"></c:out></td>
						<td><c:out value="${user.role.libelle }"></c:out></td>


					</tr>
				</c:if>

			</tbody>
		</table>
		  <a href="adminPanel" class="btn btn-primary">retour</a>
	</div>
	<script src="js/jquery-3.4.1.slim.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
