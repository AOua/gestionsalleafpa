<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid bg">
<div class="row justify-content-center">
	<div class="container-fluid col-sm-5">
		<form method="post" action="updateUserRedirect" class="form-container">

			
				<input type="hidden" class="form-control" name="id" id="id"
					value='<c:if test="${not empty id}"><c:out value="${id}" default=""></c:out></c:if>' required>
			

			<div class="form-group">
				
					<label for="nom">Nom : </label>
					<input type="text" class="form-control" name="nom" id="nom"
						value='<c:if test="${not empty nom }"><c:out value="${nom}" default=""></c:out></c:if>' required>
				
			</div>
			
			
			<div class="form-group">
				
					<label for="prenom">Prenom :</label>
					<input type="text" class="form-control" name="prenom" id="prenom"
						value='<c:if test="${not empty prenom}"><c:out value ="${prenom}" default =""> </c:out></c:if>' required>
				
			</div>
			
			
			<div class="form-group">
				
					<label for="tel">Tel :</label>
					<input type="text" class="form-control" name="tel" id="tel"
						value='<c:if test="${not empty tel}"><c:out value = "${tel}" default=""> </c:out></c:if>' required>
				
			</div>

			<div class="form-group">
				
					<label for="email">Email :</label>
					<input type="email" class="form-control" name="email" id="email"
						value='<c:if test="${not empty email}"><c:out value="${email}" default=""></c:out></c:if>' required>
				
			</div>

			<div class="form-group">
			
				<label for="centre">Centre : </label> <input type="text"
					class="form-control" name="centre" id="centre"
					value='<c:if test="${not empty idCentre}"><c:out value="${idCentre}" default=""> </c:out></c:if>' required>  				
			</div>
					
					
			<div class="form-group">
			
				<label for="numAdr">Numero adresse :</label> <input type="text"
					class="form-control" name="numAdr" id="numAdr"
					value= '<c:if test="${not empty numAdr}"><c:out value="${numAdr}" default=""></c:out></c:if>' required> 
					
			</div>
			<div class="form-group">
			
				<label for="nomAdr">Nom de la rue :</label> <input type="text"
					class="form-control" name="nomAdr" id="nomAdr"
					value='<c:if test="${not empty nomAdr}"><c:out value="${nomAdr}" default=""> </c:out></c:if>' required>			
			</div>
			
			
			<div class="form-group">
			
				<label for="cp">Code postal :</label> <input type="text"
					class="form-control" name="cp" id="cp"
					value='<c:if test="${not empty cp}"><c:out value="${cp}" default=""></c:out></c:if>'
					required>
					
			</div>
			
			
			<div class="form-group">
			
				<label for="ville">Ville :</label> <input type="text"
					class="form-control" name="ville" id="ville"
					value= '<c:if test="${not empty ville}"><c:out value="${ville}" default=""></c:out></c:if>'
					required>
					
			</div>

			<div class="form-group">
			
				<label for="login">Login :</label> <input type="text"
					class="form-control" name="login" id="login"
					value= '<c:if test="${not empty login}"><c:out value="${login}" default=""></c:out></c:if>' 
					required>
					
			</div>

			<div class="form-group">
			
				<label for="password">Mot de Passe :</label> <input type="password"
					class="form-control" name="password" id="password"
					value='<c:if test="${not empty password}"><c:out value="${password}" default=""></c:out></c:if>'
					required>					
			</div>

			<button type="submit" name="update" class="btn btn-primary">Valider</button>
		</form>
		  <a href="accueilUser.jsp" class="btn btn-primary">retour</a>
	
	</div>
	</div>
	</div>
	<script src="js/jquery-3.4.1.slim.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>