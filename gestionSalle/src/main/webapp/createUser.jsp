<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid bg">
<div class="row justify-content-center">
<div class="container-fluid col-sm-5">
	<h2>Cr�er un utilisateur !</h2>
	<form method="post" action="CreateUser" class="form-container">
	
	 <div class="form-group">
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="role" id="roleAdmin" value="1" checked>
			<label class="form-check-label" for="roleAdmin">
			   Admin
			</label>
		</div>
		<div class="form-check form-check-inline">
		    <input class="form-check-input" type="radio" name="role" id="roleUser" value="2">
		    <label class="form-check-label" for="roleUser">
		       Utilisateur
		    </label>
	    </div>
	 </div>
	  <div class="form-group">
	    <label for="nom">Nom : </label>
	    <input type="text" class="form-control" name="nom" id="nom" placeholder="Entrer votre nom" required>
	  </div>
	  <div class="form-group">
	    <label for="prenom">Prenom :</label>
	    <input type="text" class="form-control" name="prenom" id="prenom" placeholder="Entrer votre prenom" required>
	  </div>
	  <div class="form-group">
	    <label for="tel">Tel :</label>
	    <input type="text" class="form-control" name="tel" id="tel" placeholder="Entrer votre telephone" required>
	  </div>
	  <div class="form-group">
	    <label for="email">Email :</label>
	    <input type="email" class="form-control" name="email" id="email" placeholder="Entrer votre email" required>
	  </div>
	  <div class="form-group">
	    <label for="centre">Centre : </label>
	    <input type="text" class="form-control" name="centre" id="centre" placeholder="Entrer le numero du centre (1)" required>
	  </div>
	  <div class="form-group">
	    <label for="numAdr">Numero adresse :</label>
	    <input type="text" class="form-control" name="numAdr" id="numAdr" placeholder="Entrer le num�ro de la rue" required>
	  </div>
	  <div class="form-group">
	    <label for="nomAdr">Nom de la rue :</label>
	    <input type="text" class="form-control" name="nomAdr" id="nomAdr" placeholder="Entrer le nom de la rue" required>
	  </div>
	  <div class="form-group">
	    <label for="cp">Code postal :</label>
	    <input type="text" class="form-control" name="cp" id="cp" placeholder="Entrer votre code postal" required>
	  </div>
	  <div class="form-group">
	    <label for="ville">Ville :</label>
	    <input type="text" class="form-control" name="ville" id="ville" placeholder="Entrer votre ville" required>
	  </div>
	  
	  <div class="form-group">
	    <label for="login">Login :</label>
	    <input type="text" class="form-control" name="login" id="login" placeholder="Entrer votre login" required>
	  </div>
	  
	  <div class="form-group">
	    <label for="password">Mot de Passe :</label>
	    <input type="password" class="form-control" name="password" id="password" placeholder="Entrer votre mot de passe" required>
	  </div>
	  
	  <button type="submit" name="create" class="btn btn-primary">Valider</button>
	</form>
	 <a href="accueil.jsp" class="btn btn-primary">retour</a>
	</div>
	</div>
	</div>
	<script src="js/jquery-3.4.1.slim.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>

</body>
</html>