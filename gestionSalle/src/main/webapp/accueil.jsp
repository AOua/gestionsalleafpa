<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid bg">
	<div class="row justify-content-center">
		<div class="container-fluid col-sm-5">
			<h2>Bienvenue !</h2>
			<p>Coucou <c:if test="${ not empty sessionScope.holaAdmin}" ><c:out value="${ holaAdmin.prenom }" /> </c:if>, que souhaitez vous faire ?</p>
			<ul class="list-group">
			  <li class="list-group-item list-group-item-dark"><a href="createUser.jsp">Cr�er un utilisateur</a></li>
			  <li class="list-group-item list-group-item-dark"><a href="adminPanel">Lister tous les utilisateurs</a></li>
			</ul>
		</div>	
	</div>
</div>
<script src="js/jquery-3.4.1.slim.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
