<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <link href="css/bootstrap.min.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid bg">
<div class="row justify-content-center">
<div class="container-fluid col-sm-5">
	<h2>Bienvenue !</h2>
	<p>Coucou <c:if test="${ not empty holaUser}" ><c:out value="${ holaUser.nom }" /> </c:if>, que souhaitez vous faire ?</p>
	
	
	  	  <form action="voirUtilisateur" method="post">
		  	<input type="hidden" name = "id" value="${ holaUser.idPersonne }">
		    <input type="submit" class="btn btn-outline-primary" name = "voir" value="voir votre compte">
		    </form>
	 
		  <form action="updateUser" method="post">
			<input type="hidden" name = "id" value="${ holaUser.idPersonne }">
			<input type="submit" class="btn btn-outline-dark" name = "modifier" value="modifier">
		  </form>    	
</div>
</div>
</div>
<script src="js/jquery-3.4.1.slim.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
