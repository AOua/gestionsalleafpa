####################APPLICATION WEB GESTIONSALLE#########################

Etape 1)
1- Cr�er une base de donn�e nomm�e gestionSalle sur pgAdmin4 avec utilisateur postgre ainsi que votre mot de passe
2- Sur Dbeaver cr�er une nouvelle connexion postgresql en renseignant les donn�es
3- Lancer le main du projet pour cr�e vos tables personne, adresse, centre, authentification, role
4- Ouvrir le fichier "script_base_gestionSalle.sql" et ex�cut� un par un les requ�tes.
Ceci vous cr�era votre premier super user.


Fonctionnalit�)
Admin
-Vous pouvez vous connecter avec le login admin et mot de passe admin
-Vous pouvez cr�er un utilisateur (admin ou user)
-Vous pouvez acc�der � la liste des personnes
-Vous pouvez modifier une personne
-Vous pouvez supprimer une personne sauf le super user


User
-Si il y a un "user" dans la base de donn�e vous pouvez vous connecter
-Il acc�dera � la page voir ses informations et pourra modifier ses informations

